#!/usr/bin/env python

import os
import sys
import shutil

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)


def remove_file(path):
    os.remove(os.path.join(PROJECT_DIRECTORY, path))


def remove_dir(path):
    if os.path.exists(path):
        shutil.rmtree(os.path.join(PROJECT_DIRECTORY, path))


def move_file(from_path, to_path):
    os.rename(
        os.path.join(PROJECT_DIRECTORY, from_path),
        os.path.join(PROJECT_DIRECTORY, to_path),
    )

def check_values():
    app_name = '{{cookiecutter['App Name']}}'

    if ' ' in app_name:
        print('ERROR: App Name must not contain spaces.')
        sys.exit(1)

    if '_' in app_name:
        print('ERROR: App Name must not contain spaces.')
        sys.exit(1)

    if not app_name[0].isalpha():
        print('ERROR: App Name must start with an alphabetic character.')
        sys.exit(1)

if __name__ == '__main__':
    check_values()

    if '{{cookiecutter.License}}' == 'Not open source':
        remove_file('LICENSE')

    if '{{cookiecutter.Template}}' == 'C++':
        move_file('qml/Main+cpp.qml', 'qml/Main.qml')
    else:
        remove_file('main.cpp')
        remove_file('qml/qml.qrc')
        remove_file('qml/Main+cpp.qml')
        remove_dir('plugins')

    if '{{cookiecutter.Template}}' == 'Python':
        move_file('qml/Main+python.qml', 'qml/Main.qml')
    else:
        remove_file('qml/Main+python.qml')
        if '{{cookiecutter.Template}}' == 'Rust':
            remove_file('src/example.py')
        else:
            remove_dir('src')

    if '{{cookiecutter.Template}}' == 'Go':
        move_file('qml/Main+go.qml', 'qml/Main.qml')
        remove_dir('po')
        remove_file('CMakeLists.txt')
        move_file('{{cookiecutter['App Name']}}.desktop.in', '{{cookiecutter['App Name']}}.desktop')
        move_file('manifest.json.in', 'manifest.json')
    else:
        remove_file('qml/Main+go.qml')
        remove_file('main.go')

    if '{{cookiecutter.Template}}' == 'Rust':
        move_file('qml/Main+rust.qml', 'qml/Main.qml')
        remove_dir('po')
        remove_file('CMakeLists.txt')
        move_file('{{cookiecutter['App Name']}}.desktop.in', '{{cookiecutter['App Name']}}.desktop')
        move_file('manifest.json.in', 'manifest.json')
    else:
        remove_dir('.cargo')
        remove_file('qml/Main+rust.qml')
        remove_file('Cargo.toml')
        if '{{cookiecutter.Template}}' == 'Python':
            remove_file('src/build.rs')
            remove_file('src/main.rs')
            remove_file('src/qrc.rs')
        else:
            remove_dir('src')

    if '{{cookiecutter.Template}}' == 'HTML':
        remove_dir('qml')
        remove_dir('po')
        remove_file('CMakeLists.txt')
        move_file('{{cookiecutter['App Name']}}.desktop.in', '{{cookiecutter['App Name']}}.desktop')
        move_file('manifest.json.in', 'manifest.json')
    else:
        remove_dir('www')

    save_defaults = '{{cookiecutter['Save as Default']}}'.lower()
    if save_defaults == 'y' or save_defaults == 'yes':
        config_path = os.path.expanduser('~/.clickable')
        config_file = os.path.join(config_path, 'cookiecutter_config.yaml')
        if not os.path.exists(config_path):
            os.makedirs(config_path)

        with open(config_file, 'w') as f:
            f.write('default_context:\n')
            f.write('    Template: "{{cookiecutter.Template}}"\n')
            f.write('    Namespace: "{{cookiecutter.Namespace}}"\n')
            f.write('    Maintainer Name: "{{cookiecutter['Maintainer Name']|replace("'", "\\'")}}"\n')
            f.write('    Maintainer Email: "{{cookiecutter['Maintainer Email']|replace("'", "\\'")}}"\n')
            f.write('    License: "{{cookiecutter.License}}"\n')
            f.write('    Git Tag Versioning: "{{cookiecutter['Git Tag Versioning']|replace("'", "\\'")}}"\n')

        print('Defaults saved to {}'.format(config_file))

    print('\nSuccessfully created app "{{cookiecutter['App Name']|lower}}.{{cookiecutter.Namespace|lower}}"')
    print('Get started with the following commands:\n')
    print(' $ cd {}'.format(PROJECT_DIRECTORY))
    print(' $ clickable')
